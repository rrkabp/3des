#include "permutations.h"
#include "conversions.h"
#include "essentialFuncs.h"

int main() {

	// Stress Tests
	string plain1 = "Your lips are smoother than vaseline";
	string key1 = "0E329232EA6D0D73";

	string plain2 = "hello";
	string key2 = "helloWorld"; // Key length cannot be greater than 8 bytes

	string plain3 = "The quick brown fox jumps over the lazy dog";
	string key3 = "8787878787878787"; // Key needs to be padded

	string plain4 = "SendMoreMoney";
	string key4 = "ransom";

	string plain5 = "0123456789ABCDEF";
	string key5 = "133457799BBCDFF1";

	//cout << "Plaintext> " << plain1 << endl << "Key> " << key1 << endl << "CipherText> " << encode(plain1, key1) << endl << "------------------------------------------" << endl;
	//cout << "Plaintext> " << plain2 << endl << "Key> " << key2 << endl << "CipherText> " << encode(plain2, key2) << endl << "------------------------------------------" << endl;
	//cout << "Plaintext> " << plain3 << endl << "Key> " << key3 << endl << "CipherText> " << encode(plain3, key3) << endl << "------------------------------------------" << endl;
	//cout << "Plaintext> " << plain4 << endl << "Key> " << key4 << endl << "CipherText> " << encode(plain4, key4) << endl << "------------------------------------------" << endl;
	
	//string cipher = encode(plain3, key3);
	//cout << "Plaintext> " << plain3 << endl << "Key> " << key3 << endl << "CipherText> " << cipher << endl;

	//begin decryption
	//string plainResults = decode(cipher, key3);
	//cout << "PlainText> " << plainResults << "------------------------------------------" << endl;

	string userPlain = "";
	string userKey = ""; 
	string line = "";
	string userDecision = "";
	string userCipher = "";

	cout << "Encryption(E) or Decryption(D)? ";
	cin >> userDecision;

	cin.ignore();

	if (userDecision == "E") {
		cout << "Please enter the plaintext: ";
		getline(cin, userPlain);

		cout << "Please enter the key: ";
		cin >> userKey;

		string cipher3DES = tripleEncrypt(userPlain, userKey);
		cout << "3DES Plaintext> " << userPlain << endl << "Key> " << userKey << endl << "CipherText> " << cipher3DES << endl;
	} 
	else if (userDecision == "D") {
		cout << "Please enter the cipherText: ";
		getline(cin, userCipher);

		cout << "Please enter the key: ";
		cin >> userKey;

		string decrypted3DES = tripleDecrypt(userCipher, userKey);
		cout << "3DES Decrypted> " << decrypted3DES << endl;
	}
	else {
		cout << "Invalid input." << endl;
	}

	int exit = 0;
	cin >> exit;
	return 0;
}

