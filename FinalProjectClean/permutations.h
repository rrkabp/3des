#pragma once
#include <iostream>
using namespace std;

string permKey(string key, int PC[], int SIZE);									// Takes in data and table and permutates accordingly
string permutedChoice1(string key);												// Turns 64-bit key into 56-bit key w/ PC-1 Table
string *permutedChoice2(string C[], string D[]);								// Permutates 56-bit key and creates x16 48-bit keys for each round
string *permutedChoice2Decrypt(string C[], string D[]);							// Permutates 56-bit key and creates x16 48-bit keys for each round backwards
string IPMessage(string binMessage);											// Initial permutation of plaintext (binary) message
string E(string R);																// Expands each Rn-1 block in each round from 32-bits to 48-bits
string P(string sAll);															// Permutates subbed bytes and maintains 32-bit data
string finalPerm(string encodedBlock);											// Final permutation on encrypted data
string SBox(int num, int row, int column);										// Subs 8 blocks in groups of 6 from 48-bits to 32-bit