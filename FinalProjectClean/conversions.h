#pragma once
#include <iostream>
#include <sstream>
#include <bitset>
#include <vector>
using namespace std;

stringstream checkPadding(string plainHex);							// Ensures the input data is 64-bits or a multiple of 64
stringstream asciiToHex(string plaintext);							// converts ascii into hexadecimal
string hexToBin(string hex);										// converts hexadecimal to binary
char getHexCharacter(string bin);									// takes in one character and converts to hexadecimal value
string getHexText(string bin);										// converts binary to hexadecimal with the help of getHexCharacter function
int binToDec(string binstr);										// converts binary to decimal
string decToBin(int n);												// converts decimal to binary
string strXor(string str1, string str2);							// logical XOR of two strings of binary data
string leftshift(string block, int shiftBy);						// left bitwise shift of a binary string of data
string hexToAscii(string hex);										// converts hex to ascii
vector<string> splitPlain(string &str);								// Splits plaintext into blocks of 8 bit ascii (16 bits hex) for encryption
vector<string> splitCipher(string &str);							// Removes spaces from ciphertext and splits into blocks of 16 bits for decryption