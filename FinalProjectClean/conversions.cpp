#include "conversions.h"
#include <vector>

stringstream checkPadding(string plainHex) {
	stringstream plainHexReturn;
	int spacesAdded = 0;
	if (plainHex.length() > 16) {
		for (int i = 16; i < plainHex.length(); i += 16) {
			if (i % 16 == 0 && i != 0 && (i + spacesAdded < plainHex.length())) {
				plainHex.insert(i + spacesAdded, " ");							// Adds spaces every 8 Bytes to be broken into blocks later
				spacesAdded++;
			}
		}
	}
	if (plainHex.length() % 16 == 0) {											// No padding neccesary
		plainHexReturn << plainHex;
		return plainHexReturn;
	}
	else {
		while ((plainHex.length() - spacesAdded) % 16 != 0) 
			plainHex += "0";													// Adds a "0" to the end until proper length

		plainHexReturn << plainHex;
		return plainHexReturn;													// Return padded string
	}
}

vector<string> splitPlain(string &str) {
	int numOfSubStrs = ceil(str.length() / 8.0);								// Calculates the number of 8 bit (16 bits in hex) blocks of text are in the string
																				
	vector<string> returnVec;
	for (int i = 0; i < numOfSubStrs; i++)
		returnVec.push_back(str.substr(i * 8, 8));								// Creates a vector of the blocks

	return returnVec;															// Return blocks of 8 bits for encryption
}

vector<string> splitCipher(string &str) {										
	int i = 0, j = 0;
	while (str[i])																// Removes any spaces from ciphertext
	{
		if (str[i] != ' ')
			str[j++] = str[i];
		i++;
	}
	int length = str.length();
	int numOfSubStrs = ceil(j / 16.0);											// Calculates number of 16 bit blocks
	vector<string> returnVec;
	for (int i = 0; i < numOfSubStrs; i++)										// Creates vector of strings of 16 bit blocks
		returnVec.push_back(str.substr(i * 16, 16));

	return returnVec;															// Return vector of strings
}

stringstream asciiToHex(string plaintext) {
	stringstream plainHex;
	for (int i = 0; i < plaintext.length(); i++) 
		plainHex << hex << (int)plaintext[i];									// Builds HEX string
	plainHex = checkPadding(plainHex.str());									// Adds padding if neccesary
	return plainHex;															// Returns plaintext in HEX format
}

string hexToBin(string hex) {
	string bin = "";
	for (int i = 0; i < hex.length(); i++) {
		switch (toupper(hex[i])) {
		case '0': bin += "0000"; continue;
		case '1': bin += "0001"; continue;
		case '2': bin += "0010"; continue;
		case '3': bin += "0011"; continue;
		case '4': bin += "0100"; continue;
		case '5': bin += "0101"; continue;
		case '6': bin += "0110"; continue;
		case '7': bin += "0111"; continue;
		case '8': bin += "1000"; continue;
		case '9': bin += "1001"; continue;
		case 'A': bin += "1010"; continue;
		case 'B': bin += "1011"; continue;
		case 'C': bin += "1100"; continue;
		case 'D': bin += "1101"; continue;
		case 'E': bin += "1110"; continue;
		case 'F': bin += "1111"; continue;
		}
	}
	return bin;
}

char getHexCharacter(string bin) {
	if (bin.compare("1111") == 0) return 'F';
	else if (bin.compare("1110") == 0) return 'E';
	else if (bin.compare("1101") == 0) return 'D';
	else if (bin.compare("1100") == 0) return 'C';
	else if (bin.compare("1011") == 0) return 'B';
	else if (bin.compare("1010") == 0) return 'A';
	else if (bin.compare("1001") == 0) return '9';
	else if (bin.compare("1000") == 0) return '8';
	else if (bin.compare("0111") == 0) return '7';
	else if (bin.compare("0110") == 0) return '6';
	else if (bin.compare("0101") == 0) return '5';
	else if (bin.compare("0100") == 0) return '4';
	else if (bin.compare("0011") == 0) return '3';
	else if (bin.compare("0010") == 0) return '2';
	else if (bin.compare("0001") == 0) return '1';
	else if (bin.compare("0000") == 0) return '0';
	else if (bin.compare("111") == 0) return '7';
	else if (bin.compare("110") == 0) return '6';
	else if (bin.compare("101") == 0) return '5';
	else if (bin.compare("100") == 0) return '4';
	else if (bin.compare("011") == 0) return '3';
	else if (bin.compare("010") == 0) return '2';
	else if (bin.compare("001") == 0) return '1';
	else if (bin.compare("000") == 0) return '0';
	else if (bin.compare("11") == 0) return '3';
	else if (bin.compare("10") == 0) return '2';
	else if (bin.compare("01") == 0) return '1';
	else if (bin.compare("00") == 0) return '0';
	else if (bin.compare("1") == 0) return '1';
	else if (bin.compare("0") == 0) return '0';
}

string getHexText(string bin) {
	string result = "";
	for (int i = 0; i < bin.length(); i += 4)
		result += getHexCharacter(bin.substr(i, 4));
	return result;
}

string hexToAscii(string hex) {
	string ascii = "";
	for (int i = 0; i < hex.length(); i += 2) {
		string byte = hex.substr(i, 2);
		char chr = (char)(int)strtol(byte.c_str(), NULL, 16);
		ascii.push_back(chr);
	}
	return ascii;
}

int binToDec(string binstr) {
	int bin = stoi(binstr);
	int r, dec = 0, i = 0;
	while (bin != 0) {
		r = bin % 10;
		bin /= 10;
		dec += r * pow(2, i);
		++i;
	}
	return dec;
}

string decToBin(int n) {
	return bitset<4>(n).to_string();
}

string strXor(string str1, string str2) {
	string result = "";
	for (int i = 0; i < str1.length(); i++) {
		if (str1[i] == str2[i])
			result += "0";
		else
			result += "1";
	}
	return result;
}

string leftshift(string block, int shiftBy) {
	string newBlock = "";
	if (shiftBy == 1) {															// Shift block by 1
		for (int i = 1; i < block.length(); i++) {
			newBlock += block[i];
		}
		newBlock += block[0];
	}

	if (shiftBy == 2) {															// Shift block by 2
		for (int i = 2; i < block.length(); i++) {
			newBlock += block[i];
		}
		newBlock += block[0];
		newBlock += block[1];
	}
	return newBlock;															// Return new block
}


