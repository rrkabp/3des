#pragma once
#include <iostream>
#include <vector>
using namespace std;

string f(string R, string K);												// breaks data into 8 groups of 6 bits and subsititues bytes using s-box
string* makeC(string key56, int *numOfShifts);								// makes left data used for keys to encrypt/decrypt
string* makeD(string key56, int *numOfShifts);								// makes right data used for keys to encrypt/decrypt
string round(string *K, string L, string R);								// provides 16 rounds of subbing data
string roundDecode(string *K, string L, string R);							// provides 16 rounds of subbing data
string encodeBlocks(string IP, string *K);									// breaks data into Left and Right and initiates round function.
string decodeBlocks(string IP, string *K);									// breaks data into Left and Right and initiates round function backwards.
void displayInfo(string plain, string key, string plainHex,					// displays verbose information
	string hexKey, string plainBin, string binKey);		
string encrypt(string plain, string plainHex, string key);					// Encrypts current block of data
string decrypt(string cipher, string cipherHex, string key);				// Decrypts current block of data
string encode(string plain, string key);									// Encrypts all blocks of data
string decode(string cipher, string key);									// Decrypts all blocks of data
int detectKeySize(string key);												// Detects if key size is valid
string tripleEncrypt(string plain, string key);								// Creates 3DES keys and encrypts
string tripleDecrypt(string ciphertext, string key);						// Creates 3DES keys and decrypts
