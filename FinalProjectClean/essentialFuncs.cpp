#include "essentialFuncs.h"
#include "conversions.h"
#include "permutations.h"

string f(string R, string K) {
	int num, row, column;
	string *B = new string[8];
	string sboxResult, temp = "";
	string EofR = E(R);																// Expands right half from 32-bit to 48-bit
	string EofRXorK = strXor(EofR, K);												// XOR 48-bit half with current key

	// Start of grouping bytes (6 bits per group)
	// First and last bit are row; middle 4 bits are column (binary)
	B[0] = EofRXorK.substr(0, 6);
	B[1] = EofRXorK.substr(6, 6);
	B[2] = EofRXorK.substr(12, 6);
	B[3] = EofRXorK.substr(18, 6);
	B[4] = EofRXorK.substr(24, 6);
	B[5] = EofRXorK.substr(30, 6);
	B[6] = EofRXorK.substr(36, 6);
	B[7] = EofRXorK.substr(42, 6);

	for (int i = 0; i < 8; i++) {
		num = i;
		row = binToDec(B[i].substr(0, 1) + B[i].substr(5, 1));						// Finds row for current grouping (returns dec val)
		column = binToDec(B[i].substr(1, 4));										// Finds column for current grouping (returns dec val)
		sboxResult += SBox(num, row, column);										// Appends to string S-BOX value of row, column
	}

	return P(sboxResult);															// Returns permutation of S-Box string
}

string* makeC(string key56, int *numOfShifts) {
	string *C = new string[17];
	string C0 = key56.substr(0, 28);												// Seperates key into Left half
	C[0] = C0;																		// Initial left half

	for (int i = 1; i < 17; i++) {
		if (i == 1)
			C[i] = leftshift(C0, 1);												// Perform initial shift
		else
			C[i] = leftshift(C[i - 1], numOfShifts[i - 1]);							// Shift the new half using shift table
	}
	return C;																		// Return all 16 left halves
}

string* makeD(string key56, int *numOfShifts) {
	string *D = new string[17];
	string D0 = key56.substr(28, 55);												// Seperates key into Right half
	D[0] = D0;																		// Initial right half

	for (int i = 1; i < 17; i++) {
		if (i == 1)
			D[i] = leftshift(D0, 1);												// Perform intial shift
		else
			D[i] = leftshift(D[i - 1], numOfShifts[i - 1]);							// Shift the new half using shift table
	}
	return D;																		// Return all 16 right halves
}

string round(string *K, string L, string R) {
	string prevL = "";
	int ETable[48] = { 32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,12,13,12,13,14,15,16,17,16,17,18,19,20,21,20,
		21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1 };
	for (int i = 0; i < 16; i++) {
		prevL = L;																	// Stores previous Left half for reference
		L = R;																		// The new left side = the previous right side
		R = strXor(prevL, f(R, K[i]));												// Right side = Xor of (previous left half and f(Right half, and current key))
	}
	return R + L;																	// Return the halves reversed
}

string roundDecode(string *K, string L, string R) {
	string prevL = "";
	int ETable[48] = { 32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,12,13,12,13,14,15,16,17,16,17,18,19,20,21,20,
		21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1 };
	for (int i = 15; i >= 0; i--) {
		prevL = L;																	// Stores previous Left half for reference
		L = R;																		// The new left side = the previous right side
		R = strXor(prevL, f(R, K[i]));												// Right side = Xor of (previous left half and f(Right half, and current key))
	}
	return R + L;																	// Return the halves reversed
}

string encodeBlocks(string IP, string *K) {
	string L = IP.substr(0, 32);													// Left half of key
	string R = IP.substr(32, 64);													// Right half of key
	return round(K, L, R);															// Perform round (Encryption) function
}

string decodeBlocks(string IP, string *K) {
	string L = IP.substr(0, 32);													// Left half of key
	string R = IP.substr(32, 64);													// Right half of key
	return roundDecode(K, L, R);													// Perform round (Decryption) function
}

void displayInfo(string plain, string key, string plainHex, string hexKey, string plainBin, string binKey) {
	cout << "Plaintext: " << plain << endl;
	cout << "DES Key: " << key << endl;
	cout << "Plaintext (Hex): " << plainHex << endl;
	cout << "DES Key (Hex): " << hexKey << endl;
	cout << "Plaintext (Bin): " << plainBin << endl;
	cout << "DES Key (Bin): " << binKey << endl;
}

bool detectHex(string input) {
	if (input.length() % 2 != 0)													// If odd; not HEX
		return false;
	for (int i = 0; i < input.length(); i++) {
		char c = input[i];
		if (48 > int(c) || int(c) > 57) {
			if (96 > int(c) || int(c) > 102) {
				if (65 > int(c) || int(c) > 70) {
					return false;													// If within these ranges, not HEX
				}
			}
		}
	}
	return true;																	// If all else fails; we know its most likely HEX
}

string encrypt(string plain, string plainHex, string key) {
	string hexKey, plainBin, binKey, key56;
	int numOfShifts[16] = { 1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1 };						// Shift Table

	hexKey = key;																	

	plainBin = hexToBin(plainHex);													// Turns plainHEX to Binary
	binKey = hexToBin(hexKey);														// Turns HEX Key into Binary
	key56 = permutedChoice1(binKey);												// Turns 64-bit key into 56-bit

	string *C = new string[17];
	C = makeC(key56, numOfShifts);													// Makes 16 rounds of left halves
	string *D = new string[17];
	D = makeD(key56, numOfShifts);													// Makes 16 rounds of right halves
	string *K = new string[17];
	K = permutedChoice2(C, D);														// Makes 16 round keys for encryption

	string IP = IPMessage(plainBin);												// Intial permuation of plaintext
	string EB = encodeBlocks(IP, K);												// Encodes each block individually
	return getHexText(finalPerm(EB));												// Final Permutation
}

string encode(string plain, string key) {
	int index = 0;
	string strToAdd, cipherText, plainHex;
	vector<string> blocks, plainBlocks;

	// Detects if key is in HEX or ASCII; end result will be HEX
	if (!detectHex(plain))
		plainHex = asciiToHex(plain).str();
	else
		plainHex = checkPadding(plain).str();

	stringstream ss(plainHex);
	while (getline(ss, strToAdd, ' '))												// Splits each plaintext block by ' ' to be encoded
		blocks.push_back(strToAdd);

	plainBlocks = splitPlain(plain);												// Splits plaintext into blocks of 8 bit ascii

	for (auto it = blocks.begin(); it != blocks.end(); it++) {						// Encrypts each plaintext block and formats
		index = it - blocks.begin();
		if (it + 1 != blocks.end())
			cipherText += encrypt(plainBlocks[index], *it, key) + " ";
		else
			cipherText += encrypt(plainBlocks[index], *it, key);
	}
	return cipherText;																// Returns full ciphertext string
}

string decode(string cipher, string key) {
	int index = 0;
	string strToAdd, plainHex, cipherHex;
	vector<string> blocks, cipherBlocks;

	stringstream ss(cipher);

	while (getline(ss, strToAdd, ' '))												// Splits each cipher block by ' ' to be decoded
		blocks.push_back(strToAdd);

	cipherBlocks = splitCipher(cipher);												// Removes spaces and splits cipherText into 16 bits
	
	for (auto it = blocks.begin(); it != blocks.end(); it++) {
		index = it - blocks.begin();
		plainHex += decrypt(cipherBlocks[index], *it, key);							// Decrypts each cipher block into plainHEX
	}
	return plainHex;																// Return full plainHEX string
}

string decrypt(string cipher, string cipherHex, string key) {
	string hexKey, cipherBin, binKey, key56;
	int numOfShifts[16] = { 1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1 };						// Reverse shift table

	// Detects if key is in HEX or ASCII; end result will be HEX
	if (!detectHex(key))
		hexKey = asciiToHex(key).str();
	else
		hexKey = checkPadding(key).str();

	cipherBin = hexToBin(cipher);													// Turns cipher HEX to Binary
	binKey = hexToBin(hexKey);														// Turns hexKey into Binary 
	key56 = permutedChoice1(binKey);												// Converts 64-bit key to 56-bit

	string *C = new string[17];
	C = makeC(key56, numOfShifts);													// Makes 16 rounds of left halves
	string *D = new string[17];
	D = makeD(key56, numOfShifts);													// Makes 16 rounds of right halves 
	string *K = new string[17];
	K = permutedChoice2Decrypt(C, D);												// Creation of 16 keys for decryption

	string IP = IPMessage(cipherBin);												// Initial permutation of ciphertext (binary)
	string EB = decodeBlocks(IP, K);												// Decodes all ciphertext blockss
	return getHexText(finalPerm(EB));												// Turns Cipher HEX to ASCII
}

int detectKeySize(string key) {
	if (key.length() < 32 || key.length() > 48) // invalid key lengths
		return -1;
	else if (key.length() == 32) // 2 keys given
		return 2;
	else if (key.length() == 48) // 3 keys given
		return 3;
	else if (key.length() > 32 && key.length() < 48) // invlaid key length
		return -1;
	else
		return -1;

}

string tripleEncrypt(string plaintext, string key) {

	// Start of Key Creation
	string k1, k2, k3, hexKey;

	if (!detectHex(key))
		hexKey = asciiToHex(key).str();
	else
		hexKey = key;

	int keySize = detectKeySize(hexKey);

	if (keySize == -1) {
		cout << "Key size was invalid (128 or 192-bit keys)" << endl;
		_Exit(-1);
	}
	else if (keySize == 2) {
		k1 = hexKey.substr(0, 16);
		k2 = hexKey.substr(16, 16);
		k3 = k1;
	}
	else if (keySize == 3) {
		k1 = hexKey.substr(0, 16);
		k2 = hexKey.substr(16, 16);
		k3 = hexKey.substr(32, 16);
	}
	// End of Key Creation

	// 3DES Algorithm
	string A = encode(plaintext, k1);
	string B = decode(A, k2);
	return encode(B, k3);
}

string tripleDecrypt(string ciphertext, string key) {
	// Start of Key Creation
	string k1, k2, k3, hexKey;

	if (!detectHex(key))
		hexKey = asciiToHex(key).str();
	else
		hexKey = key;

	int keySize = detectKeySize(hexKey);

	if (keySize == -1) {
		cout << "Key size was invalid (128 or 192-bit keys)" << endl;
		_Exit(-1);
	}
	else if (keySize == 2) {
		k1 = hexKey.substr(0, 16);
		k2 = hexKey.substr(16, 16);
		k3 = k1;
	}
	else if (keySize == 3) {
		k1 = hexKey.substr(0, 16);
		k2 = hexKey.substr(16, 16);
		k3 = hexKey.substr(32, 16);
	}
	// End of Key Creation

	// 3DES Algorithm
	string B = decode(ciphertext, k3);
	string A = encode(B, k2);
	return hexToAscii(decode(A, k1));
}