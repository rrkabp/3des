#include <iostream>
#include <string>
#include <sstream>
#include <bitset>
using namespace std;

stringstream checkPadding(string plainHex) {
	stringstream plainHexReturn;
	int textLength = plainHex.length();
	if (textLength % 16 == 0) {
		plainHexReturn << plainHex;
		return plainHexReturn;
	}
	else {
		while (textLength % 16 != 0) {
			plainHex += "0";
			textLength++;
		}
		plainHexReturn << plainHex;
		return plainHexReturn;
	}
}

// Does not currently handle Carriage Return (0D) and Line Feed (0A), replaced with 0s as of current build.
stringstream asciiToHex(string plaintext) { 
	stringstream plainHex;
	for (int i = 0; i < plaintext.length(); i++) {
		plainHex << hex << (int)plaintext[i];
	}
	plainHex = checkPadding(plainHex.str());
	return plainHex;
	
}

string hexToBin(string hex) {
	string bin = "";
	for (int i = 0; i < hex.length(); i++) {
		switch (toupper(hex[i])) {
			case '0': bin += "0000"; continue;
			case '1': bin += "0001"; continue;
			case '2': bin += "0010"; continue;
			case '3': bin += "0011"; continue;
			case '4': bin += "0100"; continue;
			case '5': bin += "0101"; continue;
			case '6': bin += "0110"; continue;
			case '7': bin += "0111"; continue;
			case '8': bin += "1000"; continue;
			case '9': bin += "1001"; continue;
			case 'A': bin += "1010"; continue;
			case 'B': bin += "1011"; continue;
			case 'C': bin += "1100"; continue;
			case 'D': bin += "1101"; continue;
			case 'E': bin += "1110"; continue;
			case 'F': bin += "1111"; continue;
		}
	}
	return bin;
}

/*string binToHex(string bin) {
	for (size_t i = 0; i < bin.length(); i++) {
		string binToHex, temp = "0000";
		for (size_t j = 0; j < bin[i].length(); j+=4) {
			temp = bin[i].substr(j, 4);

		}
	}
}*/

char getHexCharacter(string bin)
{
	if (bin.compare("1111") == 0) return 'F';
	else if (bin.compare("1110") == 0) return 'E';
	else if (bin.compare("1101") == 0) return 'D';
	else if (bin.compare("1100") == 0) return 'C';
	else if (bin.compare("1011") == 0) return 'B';
	else if (bin.compare("1010") == 0) return 'A';
	else if (bin.compare("1001") == 0) return '9';
	else if (bin.compare("1000") == 0) return '8';
	else if (bin.compare("0111") == 0) return '7';
	else if (bin.compare("0110") == 0) return '6';
	else if (bin.compare("0101") == 0) return '5';
	else if (bin.compare("0100") == 0) return '4';
	else if (bin.compare("0011") == 0) return '3';
	else if (bin.compare("0010") == 0) return '2';
	else if (bin.compare("0001") == 0) return '1';
	else if (bin.compare("0000") == 0) return '0';
	else if (bin.compare("111") == 0) return '7';
	else if (bin.compare("110") == 0) return '6';
	else if (bin.compare("101") == 0) return '5';
	else if (bin.compare("100") == 0) return '4';
	else if (bin.compare("011") == 0) return '3';
	else if (bin.compare("010") == 0) return '2';
	else if (bin.compare("001") == 0) return '1';
	else if (bin.compare("000") == 0) return '0';
	else if (bin.compare("11") == 0) return '3';
	else if (bin.compare("10") == 0) return '2';
	else if (bin.compare("01") == 0) return '1';
	else if (bin.compare("00") == 0) return '0';
	else if (bin.compare("1") == 0) return '1';
	else if (bin.compare("0") == 0) return '0';
}

string getHexRow(string bin)
{
	string result = "";
	for (int i = 0; i < bin.length(); i = i + 4)
		result += getHexCharacter(bin.substr(i, 4));
	return result;
}

int binToDec(string binstr) {
	int bin = stoi(binstr);
	int r, dec = 0, i = 0;
	while (bin != 0) {
		r = bin % 10;
		bin /= 10;
		dec += r * pow(2, i);
		++i;
	}
	return dec;
}

string decToBin(int n) {
	return bitset<4>(n).to_string();
	/*
	while (true) {
		result = (n % 2 == 0 ? "0" : "1") + result;
		n /= 2;
	}
	return result;*/
}

// SBox1 - Sbox 8 combined
string SBox(int num, int row, int column) {
	int SBoxes[512] = { 14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7,
				0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8,
				4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0,
				15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13,
				15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10,
				3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5,
				0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15,
				13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9,
				10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8,
				13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1,
				13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7,
				1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12,
				7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15,
				13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9,
				10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4,
				3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14,
				2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9,
				14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6,
				4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14,
				11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3,
				12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11,
				10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8,
				9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6,
				4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13,
				4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1,
				13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6,
				1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2,
				6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12,
				13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7,
				1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2,
				7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8,
				2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11 };

	return decToBin(SBoxes[num*64+row*16+column]);

}

string strXor(string str1, string str2) {
	string result = "";
	for (int i = 0; i < str1.length(); i++) {
		if (str1[i] == str2[i])
			result += "0";
		else
			result += "1";
	}
	return result;
}

string permKey(string key, int PC[], int SIZE) {
	string permedKey = "";
	for (int i = 0; i < SIZE; i++)
		permedKey += key[PC[i] - 1];
	return permedKey;
}

string initialPerm(string key) {
	const int SIZE = 56;
	int PC1[SIZE] = { 57,49,41,33,25,17,9,1,58,50,42,34,26,18,10,2,59,51,43,35,27,19,11,3,60,52,44,
		36,63,55,47,39,31,23,15,7,62,54,46,38,30,22,14,6,61,53,45,37,29,21,13,5,28,20,12,4 };
	return permKey(key, PC1, SIZE);
}

string *secondPerm(string C[], string D[]) {
	const int SIZE = 48;
	string newKey = "";
	string *K = new string[16]; // K[0] == K[1] in terms of notation
	int PC2[SIZE] = { 14,17,11,24,1,5,3,28,15,6,21,10,23,19,12,4,26,8,16,7,27,20,13,2,41,52,31,37,47,
	55,30,40,51,45,33,48,44,49,39,56,34,53,46,42,50,36,29,32 };
	for (int i = 0; i < 16; i++) {
		string CD = C[i + 1] + D[i + 1];
		K[i] = permKey(CD, PC2, SIZE);
	}
	return K;
}

//takes binary input
string IPMessage(string binMessage) {
	const int SIZE = 64;
	int IP[SIZE] = { 58,50,42,34,26,18,10,2,60,52,44,36,28,20,12,4,62,54,46,38,30,22,14,6,64,56,48,40,
		32,24,16,8,57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7 };
	return permKey(binMessage, IP, SIZE);
}

string E(string R) {
	const int SIZE = 48;
	int ETable[SIZE] = { 32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,12,13,12,13,14,15,16,17,16,17,18,19,20,21,
		20,21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1 };
	return permKey(R, ETable, SIZE);
}

string P(string sAll) {
	const int SIZE = 32;
	int P[SIZE] = { 16,7,20,21,29,12,28,17,1,15,23,26,5,18,31,10,2,8,24,14,32,27,3,9,19,13,30,6,22,11,4,25 };
	return permKey(sAll, P, SIZE);
}

string finalPerm(string encodedBlock) {
	const int SIZE = 64;
	int IPinverse[SIZE] = { 40,8,48,16,56,24,64,32,39,7,47,15,55,23,63,31,38,6,46,14,54,22,62,30,37,5,45,13,53,
		21,61,29,36,4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25 };
	return permKey(encodedBlock, IPinverse, SIZE);
}

string f(string R, string K) {
	int num, row, column;
	string *B = new string[8];
	string sboxResult, temp = "";
	string EofR = E(R);
	string EofRXorK = strXor(EofR, K);
	cout << "f = " << EofRXorK << endl;
	B[0] = EofRXorK.substr(0, 6);
	B[1] = EofRXorK.substr(6, 6);
	B[2] = EofRXorK.substr(12, 6);
	B[3] = EofRXorK.substr(18, 6);
	B[4] = EofRXorK.substr(24, 6);
	B[5] = EofRXorK.substr(30, 6);
	B[6] = EofRXorK.substr(36, 6);
	B[7] = EofRXorK.substr(42, 6);

	for (int i = 0; i < 8; i++) {
		num = i;
		row = binToDec(B[i].substr(0, 1) + B[i].substr(5, 1));
		column = binToDec(B[i].substr(1, 4));
		sboxResult += SBox(num, row, column);
	}

	return P(sboxResult);
}

string leftshift(string block, int shiftBy) {
	string newBlock = "";
	if (shiftBy == 1) {
		for (int i = 1; i < block.length(); i++) {
			newBlock += block[i];
		}
		newBlock += block[0];
	}

	if (shiftBy == 2) {
		for (int i = 2; i < block.length(); i++) {
			newBlock += block[i];
		}
		newBlock += block[0];
		newBlock += block[1];
	}
	return newBlock;
}

string* makeC(string key56, int *numOfShifts) {
	string *C = new string[17];
	string C0 = key56.substr(0, 28);
	C[0] = C0;

	for (int i = 1; i < 17; i++) {
		if (i == 1) 
			C[i] = leftshift(C0, 1);
		else
			C[i] = leftshift(C[i - 1], numOfShifts[i - 1]);
	}
	return C;
}

string* makeD(string key56, int *numOfShifts) {
	string *D = new string[17];
	string D0 = key56.substr(28, 55);
	D[0] = D0;

	for (int i = 1; i < 17; i++) {
		if (i == 1)
			D[i] = leftshift(D0, 1);
		else
			D[i] = leftshift(D[i - 1], numOfShifts[i - 1]);
	}
	return D;
}

string round(string *K, string L, string R) {
	string prevL = "";
	int ETable[48] = { 32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,12,13,12,13,14,15,16,17,16,17,18,19,20,21,20,
		21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1 };
	for (int i = 0; i < 16; i++) {
		prevL = L;
		L = R;
		// R = L0 + f(R0, K1)
		// f turns Rn-1 from 32 bits to 48 bits using the selection table (ETable)
		// therefor E(Rn-1) 32 bit input and 48 bit output
		//cout << "E(R)" << E(R) << endl;
		R = strXor(prevL, f(R, K[i]));
		cout << "L> " << L << endl;
		cout << "R> " << R << endl;
	}
	return R + L;
}

string encodeBlocks(string IP, string *K) {
	string L = IP.substr(0, 32);
	string R = IP.substr(32, 64);
	//cout << "L0 = " << L << endl;
	//cout << "R0 = " << R << endl;

	return round(K, L, R);;
}

void displayInfo(string plain, string key, string plainHex, string hexKey, string plainBin, string binKey) {
	cout << "Plaintext: " << plain << endl;
	cout << "DES Key: " << key << endl;
	cout << "Plaintext (Hex): " << plainHex << endl;
	cout << "DES Key (Hex): " << hexKey << endl;
	cout << "Plaintext (Bin): " << plainBin << endl;
	cout << "DES Key (Bin): " << binKey << endl;
}

int main() {
	string plain = ""; 
	string key = "";
	int numOfShifts[16] = { 1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1 };

	cout << "Please enter in your plaintext (ASCII) > ";
	getline(cin, plain);

	cout << "Please enter in your plaintext Key (ASCII) > ";
	getline(cin, key);

	string plainHex = asciiToHex(plain).str();
	string hexKey = asciiToHex(key).str();
	string plainBin = hexToBin(plainHex);
	string binKey = hexToBin(hexKey);

	//testing
	string binKey2 = hexToBin("133457799BBCDFF1");
	displayInfo(plain, key, plainHex, hexKey, plainBin, binKey2);

	string key56 = initialPerm(binKey2);
	cout << "key56 = " << key56 << endl;

	string *C = new string[17];
	C = makeC(key56, numOfShifts);
	string *D = new string[17];
	D = makeD(key56, numOfShifts);

	//can be added to display Info
	for (int i = 0; i < 17; i++) {
		cout << "C" << i << " = " << C[i] << endl;
		cout << "D" << i << " = " << D[i] << endl;
		cout << endl << endl;
	}

	string *K = new string[17];
	K = secondPerm(C, D);

	//can be added to display info
	for (int i = 0; i < 16; i++) {
		cout << "K" << i+1 << " = " << K[i] << endl;
	}
	cout << endl << endl;

	// testing
	string IP = IPMessage("0000000100100011010001010110011110001001101010111100110111101111"); // change to actual user input
	cout << "M = 0000000100100011010001010110011110001001101010111100110111101111" << endl; 
	cout << "IP = " << IP << endl;

	string EB = encodeBlocks(IP, K);
	cout << "Final> " << finalPerm(EB) << endl;
	cout << "Final Hex> " << getHexRow(finalPerm(EB)) << endl;
	return 0;
}